/**
 * 
 */
package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.exceptions.BookIsBusyException;
import com.crossover.techtrial.exceptions.InternalError;
import com.crossover.techtrial.exceptions.IssuedLimitException;
import com.crossover.techtrial.exceptions.NotFoundException;
import com.crossover.techtrial.model.Book;
import com.crossover.techtrial.model.BookStatus;
import com.crossover.techtrial.model.Member;
import com.crossover.techtrial.model.Transaction;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author crossover
 *
 */
@Service
public class MemberServiceImpl implements MemberService {

  private MemberRepository memberRepository;
  private BookRepository bookRepository;
  private TransactionRepository transactionRepository;

  @Autowired
  public MemberServiceImpl(
      MemberRepository memberRepository,
      BookRepository bookRepository,
      TransactionRepository transactionRepository) {
    this.memberRepository = memberRepository;
    this.bookRepository = bookRepository;
    this.transactionRepository = transactionRepository;
  }

  @Transactional
  public Member save(Member member) {
    return memberRepository.save(member);
  }
  
  public Member findById(Long memberId) {
    return memberRepository.findById(memberId).orElse(null);
  }
  
  public List<Member> findAll() {
    return memberRepository.findAll();
  }

  @Override
  @Transactional
  public Transaction issueBook(Long bookId, Long memberId) {
    Book book = bookRepository.findById(bookId).orElse(null);
    Member member = memberRepository.findById(memberId).orElse(null);

    checkIssueRequest(book, member);

    Transaction transaction = new Transaction();
    transaction.setMember(member);
    transaction.setBook(book);
    book.setStatus(BookStatus.BUSY);

    return transactionRepository.save(transaction);
  }

  @Override
  @Transactional
  public Transaction returnBook(Long transactionId) {
    Transaction transaction = transactionRepository.findById(transactionId).orElse(null);

    checkReturnRequest(transaction);

    transaction.getBook().setStatus(BookStatus.FREE);
    transaction.setDateOfReturn(LocalDateTime.now());

    return transaction;
  }

  @Override
  public List<TopMemberDTO> topMembers(LocalDateTime startTime, LocalDateTime endTime) {
    return transactionRepository.getTop5Members(startTime, endTime, PageRequest.of(0, TOP_MEMBERS_LIST_SIZE));
  }

  private void checkIssueRequest(Book book, Member member) {

    if (book == null) {
      throw new NotFoundException("Book is not found.");
    }

    if (book.getStatus() == BookStatus.BUSY) {
      throw new BookIsBusyException("This book is already issued.");
    }

    if (member == null) {
      throw new NotFoundException("Member is not found");
    }

    if (transactionRepository.countTransactionByMemberAndDateOfReturnNull(member) >= MAX_ONGOING_TRANSACTIONS) {
      throw new IssuedLimitException("Maximum of 5 simultaneous issued books exceeded.");
    }
  }

  private void checkReturnRequest(Transaction transaction) {
    if (transaction == null) {
      throw new NotFoundException("Transaction is not found");
    }

    if (transaction.getDateOfReturn() != null) {
      throw new InternalError("Transaction is already completed");
    }
  }
}
