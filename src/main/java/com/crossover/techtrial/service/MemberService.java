/**
 * 
 */
package com.crossover.techtrial.service;

import java.time.LocalDateTime;
import java.util.List;

import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.model.Member;
import com.crossover.techtrial.model.Transaction;

/**
 * RideService for rides.
 * @author crossover
 *
 */
public interface MemberService {

  int MAX_ONGOING_TRANSACTIONS = 5;
  int TOP_MEMBERS_LIST_SIZE = 5;
  
  Member save(Member member);
  
  Member findById(Long memberId);
  
  List<Member> findAll();

  Transaction issueBook(Long bookId, Long memberId);

  Transaction returnBook(Long transactionId);

  List<TopMemberDTO> topMembers(LocalDateTime startTime, LocalDateTime endTime);
}
