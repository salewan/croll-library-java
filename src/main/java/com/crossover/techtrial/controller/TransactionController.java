/**
 * 
 */
package com.crossover.techtrial.controller;

import com.crossover.techtrial.model.Transaction;
import com.crossover.techtrial.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author kshah
 *
 */
@RestController
public class TransactionController {

  private MemberService memberService;

  @Autowired
  public TransactionController(MemberService memberService) {
    this.memberService = memberService;
  }

  /*
   * PLEASE DO NOT CHANGE SIGNATURE OR METHOD TYPE OF END POINTS
   * Example Post Request :  { "bookId":1,"memberId":33 }
   */
  @PostMapping(path = "/api/transaction")
  public ResponseEntity<Transaction> issueBookToMember(@RequestBody Map<String, Long> params) {
    
    Long bookId = params.get("bookId");
    Long memberId = params.get("memberId");

    return ResponseEntity.ok().body(memberService.issueBook(bookId, memberId));
  }

  /*
   * PLEASE DO NOT CHANGE SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @PatchMapping(path= "/api/transaction/{transaction-id}/return")
  public ResponseEntity<Transaction> returnBookTransaction(@PathVariable(name="transaction-id") Long transactionId) {
    return ResponseEntity.ok().body(memberService.returnBook(transactionId));
  }

}
