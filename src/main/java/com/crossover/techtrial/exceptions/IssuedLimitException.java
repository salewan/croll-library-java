package com.crossover.techtrial.exceptions;

public class IssuedLimitException extends RuntimeException {

  public IssuedLimitException(String message) {
    super(message);
  }
}
