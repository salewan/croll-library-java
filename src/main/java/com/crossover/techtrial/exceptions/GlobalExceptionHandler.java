package com.crossover.techtrial.exceptions;

import java.util.AbstractMap;
import java.util.Date;

import com.crossover.techtrial.dto.ErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Component
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  /**
   * Global Exception handler for all exceptions.
   */
  @ExceptionHandler
  public ResponseEntity<AbstractMap.SimpleEntry<String, String>> handle(Exception exception) {
    // general exception
    LOG.error("Exception: Unable to process this request. ", exception);
    AbstractMap.SimpleEntry<String, String> response =
        new AbstractMap.SimpleEntry<>("message", "Unable to process this request.");
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request
  ) {
    ErrorDetails errorDetails = new ErrorDetails(new Date(), "Validation Failed",
        ex.getBindingResult().toString());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDetails);
  }


  @ExceptionHandler(IssuedLimitException.class)
  public ResponseEntity<ErrorDetails> handleIssuedLimitException(Exception ex, WebRequest request) {
    LOG.error(ex.getMessage(), ex);

    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
        request.getDescription(false));

    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errorDetails);
  }

  @ExceptionHandler(NotFoundException.class)
  public final ResponseEntity<ErrorDetails> handleNotFoundException(Exception ex, WebRequest request) {
    LOG.error(ex.getMessage(), ex);

    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
        request.getDescription(false));

    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorDetails);
  }

  @ExceptionHandler(BookIsBusyException.class)
  public ResponseEntity<ErrorDetails> handleBookIsBusyException(Exception ex, WebRequest request) {
    LOG.error(ex.getMessage(), ex);

    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
        request.getDescription(false));

    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errorDetails);
  }

  @ExceptionHandler(InternalError.class)
  public ResponseEntity<ErrorDetails> handleInternalError(Exception ex, WebRequest request) {
    LOG.error(ex.getMessage(), ex);

    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
        request.getDescription(false));

    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errorDetails);
  }

}