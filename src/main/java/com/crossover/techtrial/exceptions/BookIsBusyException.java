package com.crossover.techtrial.exceptions;

public class BookIsBusyException extends RuntimeException {

  public BookIsBusyException(String message) {
    super(message);
  }
}
