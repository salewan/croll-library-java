package com.crossover.techtrial.exceptions;

public class InternalError extends RuntimeException {

  public InternalError(String message) {
    super(message);
  }
}
