package com.crossover.techtrial.model;

public enum BookStatus {

    BUSY, FREE
}
