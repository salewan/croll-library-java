/**
 * 
 */
package com.crossover.techtrial.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

/**
 * @author kshah
 *
 */
@Entity
@Table(name = "member")
public class Member implements Serializable {
  
  private static final long serialVersionUID = 9045098179799205444L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  @Pattern(regexp = "^\\p{L}.{1,99}", message = "name should have at least 2 symbols and start with an alphabet")
  private String name;

  @Column(name = "email", unique = true)
  private String email;
  
  @Enumerated(EnumType.STRING)
  private MembershipStatus membershipStatus;
  
  @Column(name = "membership_start_date")
  private LocalDateTime membershipStartDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public MembershipStatus getMembershipStatus() {
    return membershipStatus;
  }

  public void setMembershipStatus(MembershipStatus membershipStatus) {
    this.membershipStatus = membershipStatus;
  }

  public LocalDateTime getMembershipStartDate() {
    return membershipStartDate;
  }

  public void setMembershipStartDate(LocalDateTime membershipStartDate) {
    this.membershipStartDate = membershipStartDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Member member = (Member) o;
    return Objects.equals(id, member.id) &&
        Objects.equals(name, member.name) &&
        Objects.equals(email, member.email) &&
        membershipStatus == member.membershipStatus &&
        Objects.equals(membershipStartDate, member.membershipStartDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, email, membershipStatus, membershipStartDate);
  }

  @Override
  public String toString() {
    return "Member [" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", membershipStatus=" + membershipStatus +
        ", membershipStartDate=" + membershipStartDate +
        ']';
  }
}
