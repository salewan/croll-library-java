/**
 * 
 */
package com.crossover.techtrial.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author kshah
 *
 */
@Entity
@Table(name="transaction")
public class Transaction implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8951221480021840448L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @OneToOne
  @JoinColumn(name = "book_id", referencedColumnName = "id", nullable = false)
  private Book book;
  
  @OneToOne
  @JoinColumn(name="member_id", referencedColumnName="id", nullable = false)
  private Member member;
  //Date and time of issuance of this book
  @Column(name="date_of_issue", nullable = false)
  @CreationTimestamp
  private LocalDateTime dateOfIssue;
  
  //Date and time of return of this book
  @Column(name="date_of_return")
  private LocalDateTime dateOfReturn;

  @Version
  @Column
  private Long version;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public Member getMember() {
    return member;
  }

  public void setMember(Member member) {
    this.member = member;
  }

  public LocalDateTime getDateOfIssue() {
    return dateOfIssue;
  }

  public void setDateOfIssue(LocalDateTime dateOfIssue) {
    this.dateOfIssue = dateOfIssue;
  }

  public LocalDateTime getDateOfReturn() {
    return dateOfReturn;
  }

  public void setDateOfReturn(LocalDateTime dateOfReturn) {
    this.dateOfReturn = dateOfReturn;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Transaction that = (Transaction) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(dateOfIssue, that.dateOfIssue) &&
        Objects.equals(dateOfReturn, that.dateOfReturn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dateOfIssue, dateOfReturn);
  }

  @Override
  public String toString() {
    return "Transaction [" +
        "id=" + id +
        ", book=" + book +
        ", member=" + member +
        ", dateOfIssue=" + dateOfIssue +
        ", dateOfReturn=" + dateOfReturn +
        ", version=" + version +
        ']';
  }
}
