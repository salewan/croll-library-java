/**
 * 
 */
package com.crossover.techtrial.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author kshah
 *
 */
@Entity
@Table(name = "book")
public class Book implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -5241781253380015253L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "title")
  private String title;

  @Version
  @Column
  private Long version;

  @Enumerated(EnumType.STRING)
  private BookStatus status = BookStatus.FREE;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public BookStatus getStatus() {
    return status;
  }

  public void setStatus(BookStatus status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Book book = (Book) o;
    return Objects.equals(id, book.id) &&
        Objects.equals(title, book.title) &&
        status == book.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, title, status);
  }

  @Override
  public String toString() {
    return "Book [" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", version=" + version +
        ", status=" + status +
        ']';
  }
}
