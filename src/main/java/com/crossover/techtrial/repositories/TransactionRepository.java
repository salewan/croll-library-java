/**
 * 
 */
package com.crossover.techtrial.repositories;

import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.model.Member;
import com.crossover.techtrial.model.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author crossover
 *
 */
@RestResource(exported = false)
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

  @Query("select new com.crossover.techtrial.dto.TopMemberDTO(t.member.id, t.member.name, t.member.email, count(t.member)) " +
      "from Transaction t " +
      "inner join Member m on t.member = m " +
      "where t.dateOfIssue between ?1 and ?2 and t.dateOfReturn between ?1 and ?2 " +
      "group by t.member " +
      "order by count(t.member) desc")
  List<TopMemberDTO> getTop5Members(LocalDateTime startTime, LocalDateTime endTime, Pageable pageable);

  Long countTransactionByMemberAndDateOfReturnNull(Member member);
}
