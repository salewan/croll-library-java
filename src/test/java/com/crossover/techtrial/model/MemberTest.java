package com.crossover.techtrial.model;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MemberTest {

  private LocalDateTime now = LocalDateTime.now();

  private Member createFakeMember() {
    Member member = new Member();
    member.setId(1L);
    member.setName("fake member");
    member.setEmail("example000000000000001@gmail.com");
    member.setMembershipStartDate(now);
    member.setMembershipStatus(MembershipStatus.ACTIVE);
    return member;
  }

  @Test
  public void memberAlwaysShouldBeEqualWithItself() {
    Member member = createFakeMember();
    assertEquals(member, member);
  }

  @Test
  public void memberEqualsTransitivity() {
    Member a = createFakeMember();
    Member b = createFakeMember();
    assertEquals(a, b);
    assertEquals(b, a);
  }

  @Test
  public void memberEqualsConsistency() {
    Member a = createFakeMember();
    Member b = createFakeMember();
    assertEquals(a, b);

    b.setName("another");
    assertNotEquals(a, b);
  }

  @Test
  public void memberEqualsShouldBeFalseWithNull() {
    Member a = createFakeMember();
    assertNotEquals(a, null);
  }

  @Test
  public void memberTwoEqualObjectsShouldHaveSameHashcode() {
    Member a = createFakeMember();
    Member b = createFakeMember();
    assertEquals(a, b);
    assertEquals(a.hashCode(), b.hashCode());
  }
}