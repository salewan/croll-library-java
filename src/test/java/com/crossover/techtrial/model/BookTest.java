package com.crossover.techtrial.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class BookTest {

  private Book createFakeBook() {
    Book book = new Book();
    book.setId(1L);
    book.setTitle("abc");
    book.setStatus(BookStatus.FREE);
    return book;
  }

  @Test
  public void bookAlwaysShouldBeEqualWithItself() {
    Book book = new Book();
    assertEquals(book, book);
  }

  @Test
  public void bookEqualsTransitivity() {
    Book a = createFakeBook();
    Book b = createFakeBook();
    assertEquals(a, b);
    assertEquals(b, a);
  }

  @Test
  public void bookEqualsConsistency() {
    Book a = createFakeBook();
    Book b = createFakeBook();
    assertEquals(a, b);

    b.setTitle("abc?");
    assertNotEquals(a, b);
  }

  @Test
  public void bookEqualsShouldBeFalseWithNull() {
    Book a = new Book();
    a.setId(1L);
    assertNotEquals(a, null);
  }

  @Test
  public void bookTwoEqualObjectsShouldHaveSameHashcode() {
    Book a = createFakeBook();
    Book b = createFakeBook();
    assertEquals(a, b);
    assertEquals(a.hashCode(), b.hashCode());
  }
}