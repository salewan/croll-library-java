package com.crossover.techtrial.model;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TransactionTest {

  private LocalDateTime now = LocalDateTime.now();

  private Transaction createFakeTransaction() {
    Transaction transaction = new Transaction();
    transaction.setId(1L);
    transaction.setDateOfIssue(now);
    transaction.setDateOfReturn(null);
    return transaction;
  }

  @Test
  public void transactionAlwaysShouldBeEqualWithItself() {
    Transaction transaction = createFakeTransaction();
    assertEquals(transaction, transaction);
  }

  @Test
  public void transactionEqualsTransitivity() {
    Transaction a = createFakeTransaction();
    Transaction b = createFakeTransaction();
    assertEquals(a, b);
    assertEquals(b, a);
  }

  @Test
  public void transactionEqualsConsistency() {
    Transaction a = createFakeTransaction();
    Transaction b = createFakeTransaction();
    assertEquals(a, b);

    b.setDateOfReturn(now);
    assertNotEquals(a, b);
  }

  @Test
  public void transactionEqualsShouldBeFalseWithNull() {
    Transaction a = createFakeTransaction();
    assertNotEquals(a, null);
  }

  @Test
  public void transactionTwoEqualObjectsShouldHaveSameHashcode() {
    Transaction a = createFakeTransaction();
    Transaction b = createFakeTransaction();
    assertEquals(a, b);
    assertEquals(a.hashCode(), b.hashCode());
  }
}