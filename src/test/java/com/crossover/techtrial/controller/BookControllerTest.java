package com.crossover.techtrial.controller;

import com.crossover.techtrial.model.Book;
import com.crossover.techtrial.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private BookService bookService;

  TestEntityManager testEntityManager;

  @Autowired
  private ObjectMapper mapper;

  @Test
  public void getBooksShouldReturnListOfBooks() throws Exception {
    Book book = new Book();
    book.setId(2L);
    book.setTitle("abc");
    when(bookService.getAll()).thenReturn(Collections.singletonList(book));

    mockMvc.perform(get("/api/book"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(Collections.singletonList(book)))));
  }

  @Test
  public void saveBookShouldReturnSavedBook() throws Exception {
    Book book = new Book();
    book.setId(1L);
    book.setTitle("cba");

    when(bookService.save(book)).thenReturn(book);

    mockMvc.perform(
        post("/api/book")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(book)))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(book))));
  }

  @Test
  public void getRideByIdShouldReturnRequestedInstanceIfItExists() throws Exception {
    Book book = new Book();
    book.setId(1L);
    book.setTitle("cba");

    when(bookService.findById(1L)).thenReturn(book);

    mockMvc.perform(get("/api/book/{book-id}", 1L))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(book))));
  }

  @Test
  public void getRideByIdShouldReturnNotFoundIfRequestedInstanceAbsent() throws Exception {
    when(bookService.findById(anyLong())).thenReturn(null);
    mockMvc.perform(get("/api/book/{book-id}", 1L))
        .andExpect(status().isNotFound());
  }
}
