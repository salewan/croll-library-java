package com.crossover.techtrial.controller;

import com.crossover.techtrial.model.Transaction;
import com.crossover.techtrial.service.MemberService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private MemberService memberService;

  @Autowired
  private ObjectMapper mapper;

  @Test
  public void issueBookToMemberShouldReturnTransaction() throws Exception {
    Transaction transaction = new Transaction();
    transaction.setId(100L);
    when(memberService.issueBook(1L, 1L)).thenReturn(transaction);
    Map<String, Long> params = new HashMap<>();
    params.put("bookId", 1L);
    params.put("memberId", 1L);

    mockMvc.perform(
        post("/api/transaction")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(params)))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(transaction))));
  }

  @Test
  public void returnBookTransactionShouldReturnTransactionObject() throws Exception {
    Transaction transaction = new Transaction();
    transaction.setId(100L);
    when(memberService.returnBook(100L)).thenReturn(transaction);

    mockMvc.perform(patch("/api/transaction/{transaction-id}/return", 100L))
        .andExpect(status().isOk())
        .andExpect(content().string(mapper.writeValueAsString(transaction)));
  }
}
