/**
 * 
 */
package com.crossover.techtrial.controller;

import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.model.Member;
import com.crossover.techtrial.service.MemberService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author kshah
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(MemberController.class)
public class MemberControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private MemberService memberService;

  @Autowired
  private ObjectMapper mapper;

  @Test
  public void registerShouldReturnRegisteredMember() throws Exception {
    Member member = new Member();
    member.setId(1L);
    member.setName("ab");
    when(memberService.save(member)).thenReturn(member);

    mockMvc.perform(
    post("/api/member")
        .contentType(MediaType.APPLICATION_JSON)
        .content(mapper.writeValueAsString(member)))
    .andExpect(status().isOk())
    .andExpect(content().string(containsString(mapper.writeValueAsString(member))));
  }

  @Test
  public void getAllShouldReturnListOfMembers() throws Exception {
    Member member = new Member();
    member.setId(1L);
    member.setName("ab");
    when(memberService.findAll()).thenReturn(Collections.singletonList(member));

    mockMvc.perform(get("/api/member"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(Collections.singletonList(member)))));
  }

  @Test
  public void getMemberByIdWhenExistsMemberShouldReturnIt() throws Exception {
    Member member = new Member();
    member.setId(1L);
    member.setName("ab");
    when(memberService.findById(1L)).thenReturn(member);

    mockMvc.perform(get("/api/member/{member-id}", 1L))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(member))));
  }

  @Test
  public void getMemberByIdWhetAbsenceMemberShouldReturnNotFound() throws Exception {
    when(memberService.findById(2L)).thenReturn(null);

    mockMvc.perform(get("/api/member/{member-id}", 2L))
        .andExpect(status().isNotFound());
  }

  @Test
  public void getTopMembersShouldReturnListOfTopMembers() throws Exception {
    TopMemberDTO dto = new TopMemberDTO();
    dto.setMemberId(1L);
    dto.setBookCount(1001);
    LocalDateTime startTime = LocalDateTime.now();
    LocalDateTime endTime = LocalDateTime.now();

    when(memberService.topMembers(any(LocalDateTime.class), any(LocalDateTime.class)))
        .thenReturn(Collections.singletonList(dto));

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MemberController.DATE_FORMAT);
    String fromParam = startTime.format(formatter);
    String toParam = endTime.format(formatter);
    mockMvc.perform(
        get("/api/member/top-member")
            .param("startTime", fromParam)
            .param("endTime", toParam))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(mapper.writeValueAsString(Collections.singletonList(dto)))));
  }
}
