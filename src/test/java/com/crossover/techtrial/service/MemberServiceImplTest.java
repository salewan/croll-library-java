package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.exceptions.BookIsBusyException;
import com.crossover.techtrial.exceptions.InternalError;
import com.crossover.techtrial.exceptions.IssuedLimitException;
import com.crossover.techtrial.exceptions.NotFoundException;
import com.crossover.techtrial.model.Book;
import com.crossover.techtrial.model.BookStatus;
import com.crossover.techtrial.model.Member;
import com.crossover.techtrial.model.Transaction;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TransactionRepository;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberServiceImplTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @MockBean
  private MemberRepository memberRepository;

  @MockBean
  private BookRepository bookRepository;

  @MockBean
  private TransactionRepository transactionRepository;

  private MemberService memberService;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    this.memberService = new MemberServiceImpl(memberRepository, bookRepository, transactionRepository);
  }

  @Test
  public void issueBookWhenEverythingOkShouldCreateTransaction() {
    Book book = new Book();
    book.setStatus(BookStatus.FREE);
    Member member = new Member();
    Transaction transaction = new Transaction();
    given(bookRepository.findById(anyLong())).willReturn(Optional.of(book));
    given(memberRepository.findById(anyLong())).willReturn(Optional.of(member));
    given(transactionRepository.save(any(Transaction.class))).willReturn(transaction);
    given(transactionRepository.countTransactionByMemberAndDateOfReturnNull(member))
        .willReturn((long) MemberService.MAX_ONGOING_TRANSACTIONS - 1);

    Transaction actual = memberService.issueBook(1L, 1L);
    assertThat(actual, Matchers.equalTo(transaction));
  }

  @Test
  public void issueBookWhenBookAbsenceShouldThrowNotFoundException() {
    given(bookRepository.findById(anyLong())).willReturn(Optional.empty());
    given(memberRepository.findById(anyLong())).willReturn(Optional.of(new Member()));
    thrown.expect(NotFoundException.class);
    memberService.issueBook(1L, 2L);

  }

  @Test
  public void issueBookWhenMemberAbsenceShouldThrowNotFoundException() {
    given(bookRepository.findById(anyLong())).willReturn(Optional.of(new Book()));
    given(memberRepository.findById(anyLong())).willReturn(Optional.empty());
    thrown.expect(NotFoundException.class);
    memberService.issueBook(2L, 1L);
  }

  @Test
  public void issueBookWhenBookIsBusyShouldThrowBookIsBusyException() {
    Book book = new Book();
    book.setStatus(BookStatus.BUSY);
    given(bookRepository.findById(anyLong())).willReturn(Optional.of(book));
    given(memberRepository.findById(anyLong())).willReturn(Optional.of(new Member()));
    thrown.expect(BookIsBusyException.class);
    memberService.issueBook(1L, 1L);
  }

  @Test
  public void issueBookWhenMemberHasManyOngoingTransactionsShouldThrowIssuedLimitException() {
    Book book = new Book();
    book.setStatus(BookStatus.FREE);
    Member member = new Member();
    Transaction transaction = new Transaction();
    given(bookRepository.findById(anyLong())).willReturn(Optional.of(book));
    given(memberRepository.findById(anyLong())).willReturn(Optional.of(member));
    given(transactionRepository.save(any(Transaction.class))).willReturn(transaction);
    given(transactionRepository.countTransactionByMemberAndDateOfReturnNull(member))
        .willReturn((long) MemberService.MAX_ONGOING_TRANSACTIONS);

    thrown.expect(IssuedLimitException.class);
    memberService.issueBook(1L, 1L);
  }

  @Test
  public void returnBookWhenEverythingOkShouldReturnCompletedTransaction() {
    Book book = new Book();
    book.setStatus(BookStatus.BUSY);
    Transaction transaction = new Transaction();
    transaction.setBook(book);
    LocalDateTime now = LocalDateTime.now();
    transaction.setDateOfIssue(now);
    transaction.setDateOfReturn(null);

    given(transactionRepository.findById(anyLong())).willReturn(Optional.of(transaction));
    Transaction actual = memberService.returnBook(1L);
    assertThat(actual.getDateOfIssue(), Matchers.equalTo(now));
    assertThat(actual.getDateOfReturn(), Matchers.notNullValue());
    assertThat(actual.getBook().getStatus(), Matchers.equalTo(BookStatus.FREE));
  }

  @Test
  public void returnBookWhenTransactionAbsentShouldThrowNotFoundException() {
    given(transactionRepository.findById(anyLong())).willReturn(Optional.empty());
    thrown.expect(NotFoundException.class);
    memberService.returnBook(1L);
  }

  @Test
  public void returnBookWhenTransactionAlreadyCompletedShouldThrowException() {
    Transaction transaction = new Transaction();
    transaction.setDateOfIssue(LocalDateTime.now());
    transaction.setDateOfReturn(LocalDateTime.now());
    given(transactionRepository.findById(anyLong())).willReturn(Optional.of(transaction));
    thrown.expect(InternalError.class);
    memberService.returnBook(1L);
  }

  @Test
  public void findAllAlwaysShouldReturnRepositoryHave() {
    Member member = new Member();
    member.setId(100L);
    member.setEmail("example123@gmail.com");
    given(memberRepository.findAll()).willReturn(Collections.singletonList(member));
    List<Member> actualList = memberService.findAll();
    assertEquals(1, actualList.size());
    assertThat(actualList.get(0), Matchers.equalTo(member));

    given(memberRepository.findAll()).willReturn(Collections.emptyList());
    List<Member> emptyList = memberService.findAll();
    assertTrue(emptyList.isEmpty());
  }

  @Test
  public void findByIdAlwaysShouldReturnEntityIfItExists() {
    Member member = new Member();
    member.setId(1L);
    given(memberRepository.findById(1L)).willReturn(Optional.of(member));
    Member actual = memberService.findById(1L);
    assertEquals(member, actual);
    Member nobody = memberService.findById(2L);
    assertNull(nobody);
  }

  @Test
  public void saveAlwaysPropagatesFunctionToRepository() {
    Member member = new Member();
    member.setId(1L);
    given(memberRepository.save(member)).willReturn(member);
    Member actual = memberService.save(member);
    assertEquals(actual, member);
  }

  @Test
  public void topMembersAlwaysFiresUpAppropriateQuery() {
    LocalDateTime startTime = LocalDateTime.now();
    LocalDateTime endTime = LocalDateTime.now();
    TopMemberDTO dto = new TopMemberDTO();
    dto.setMemberId(2L);
    dto.setBookCount(100);
    given(
        transactionRepository
            .getTop5Members(startTime, endTime, PageRequest.of(0, MemberService.TOP_MEMBERS_LIST_SIZE)))
        .willReturn(Collections.singletonList(dto));
    List<TopMemberDTO> topMember = memberService.topMembers(startTime, endTime);
    assertEquals(1, topMember.size());
    assertEquals(dto, topMember.get(0));
  }
}